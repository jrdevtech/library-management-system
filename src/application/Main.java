package application;

import java.awt.EventQueue;

public class Main {

	/**
	 * Start Application
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppGui appGui = new AppGui();
					appGui.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		}

}
