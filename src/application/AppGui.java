package application;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.sql.*;
import java.util.UUID;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.JPanel;
import java.awt.CardLayout;
public class AppGui {

	public JFrame frame;
	private JTextField txtName;
	private JTextField txtDob;
	private JTextField txtPhone;
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	Connection connection = null;
	private JTextField txtLoginUsername;
	private JPasswordField txtLoginPassword;
	private boolean isLoggedIn;
	/**
	 * Create the application.
	 */

	public AppGui() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
		JPanel pnlFirstUse = new JPanel();
		frame.getContentPane().add(pnlFirstUse, "name_24137823203700");
		pnlFirstUse.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(139, 28, 66, 15);
		pnlFirstUse.add(lblName);
		
		JLabel lblDob = new JLabel("DOB");
		lblDob.setBounds(139, 55, 66, 15);
		pnlFirstUse.add(lblDob);
		
		JLabel lblPhone = new JLabel("Phone");
		lblPhone.setBounds(139, 82, 66, 15);
		pnlFirstUse.add(lblPhone);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(139, 109, 78, 15);
		pnlFirstUse.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(139, 136, 66, 15);
		pnlFirstUse.add(lblPassword);
		
		txtName = new JTextField();
		txtName.setBounds(223, 26, 124, 19);
		pnlFirstUse.add(txtName);
		txtName.setColumns(10);
		
		txtDob = new JTextField();
		txtDob.setColumns(10);
		txtDob.setBounds(223, 53, 124, 19);
		pnlFirstUse.add(txtDob);
		
		txtPhone = new JTextField();
		txtPhone.setBounds(223, 80, 124, 19);
		pnlFirstUse.add(txtPhone);
		txtPhone.setColumns(10);
		
		txtUsername = new JTextField();
		txtUsername.setColumns(10);
		txtUsername.setBounds(223, 107, 124, 19);
		pnlFirstUse.add(txtUsername);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(223, 136, 124, 19);
		pnlFirstUse.add(txtPassword);
		
		JButton btnSignUpAdmin = new JButton("Sign Up As Admin");
		btnSignUpAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				PreparedStatement prep = null;
				try {
					String query = "INSERT INTO newuser"
							+ "(name, dob, phonenumber, username, password, uuid)"
							+ "VALUES (?,?,?,?,?,?)";
					connection = databaseConnection.dataConnector();
					prep = connection.prepareStatement(query);
					prep.setString(1, txtName.getText());
					prep.setString(2, txtDob.getText());
					prep.setString(3, txtPhone.getText());
					prep.setString(4, txtUsername.getText());
					prep.setString(5, txtPassword.getPassword().toString());
					prep.setString(6, UUID.randomUUID().toString());
					prep.executeUpdate();
					
				}catch(Exception e) {JOptionPane.showMessageDialog(null,e);}
				finally{
					try {
						if(prep != null) {
							prep.close();
						}
						if(connection != null) {
							connection.close();
						}
					} catch (SQLException e) {
						JOptionPane.showMessageDialog(null, e);
					}
				}
			}
		});
		btnSignUpAdmin.setBounds(148, 200, 157, 25);
		pnlFirstUse.add(btnSignUpAdmin);
		
		JPanel pnlLogin = new JPanel();
		frame.getContentPane().add(pnlLogin, "name_26564363315323");
		pnlLogin.setLayout(null);
		
		JLabel lblLogin = new JLabel("Login Page");
		lblLogin.setBounds(181, 38, 83, 15);
		pnlLogin.add(lblLogin);
		
		JLabel lblUsername_1 = new JLabel("Username");
		lblUsername_1.setBounds(122, 95, 78, 15);
		pnlLogin.add(lblUsername_1);
		
		JLabel lblPassword_1 = new JLabel("Password");
		lblPassword_1.setBounds(122, 176, 78, 15);
		pnlLogin.add(lblPassword_1);
		
		JButton btnNewButton = new JButton("Login");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PreparedStatement prep = null;
				
				int correct = 0;

				try {
					
					String query = "SELECT * FROM newuser WHERE username=? AND password=?";
					connection = databaseConnection.dataConnector();
					prep = connection.prepareStatement(query);
					prep.setString(1, txtLoginUsername.getText());
					prep.setString(2, txtLoginPassword.getPassword().toString());
					ResultSet rs = prep.executeQuery();
					while(rs.next()){
						JOptionPane.showMessageDialog(null,rs.getString("username"));
						JOptionPane.showMessageDialog(null, rs.getString("password"));
						if(correct == 2) {isLoggedIn = true; JOptionPane.showMessageDialog(null,"You Are Logged In");}
						else if(correct != 2) {JOptionPane.showMessageDialog(null, "Invalid Login Credentials");}
					}
					JOptionPane.showMessageDialog(null, rs.getString("username"));
					
				}catch (SQLException e) {
					JOptionPane.showMessageDialog(null,e);
				}
				finally {
					try {
						
						if(prep != null) {
							prep.close();
						}
						if(connection != null) {
							connection.close();
						}
					}catch(Exception e) {JOptionPane.showMessageDialog(null,e);}
				}
			}
		});
		btnNewButton.setBounds(12, 234, 424, 25);
		pnlLogin.add(btnNewButton);
		
		txtLoginUsername = new JTextField();
		txtLoginUsername.setBounds(218, 93, 124, 19);
		pnlLogin.add(txtLoginUsername);
		txtLoginUsername.setColumns(10);
		
		txtLoginPassword = new JPasswordField();
		txtLoginPassword.setBounds(218, 174, 124, 19);
		pnlLogin.add(txtLoginPassword);
		if(isFirstUse() &&(pnlFirstUse.isVisible() == false)) {if(pnlLogin.isVisible()) {pnlLogin.setVisible(false);}pnlFirstUse.setVisible(true);}
		else if(isFirstUse() != true) {if(pnlFirstUse.isVisible()) {pnlFirstUse.setVisible(false);}pnlLogin.setVisible(true);}
	}
	 boolean isFirstUse() {
		try {
			String query = "SELECT COUNT(*) FROM newuser";
			connection = databaseConnection.dataConnector();
			PreparedStatement prep = connection.prepareStatement(query);
			ResultSet rs = prep.executeQuery();
			if(rs.getInt(1) == 0) {prep.close();connection.close();return true;}
			prep.close();
			connection.close();
			return false;
		}catch(Exception e){JOptionPane.showMessageDialog(null, e);}
		return false;
	}
}

